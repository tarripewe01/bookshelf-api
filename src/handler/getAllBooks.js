const books = require("../books");

const getAllBooksHandler = (request, h) => {
  // const allBook = books.map((book) => ({
  //   id: book.id,
  //   name: book.name,
  //   publisher: book.publisher,
  // }));

  // const response = h.response({
  //   status: "success",
  //   data: {
  //     books: allBook,
  //   },
  // });
  // response.code(200);
  // return response;
  const { name, reading, finished } = request.query;

  if (name) {
    return {
      status: "success",
      data: {
        books: books
          .filter((book) =>
            book.name.toLowerCase().includes(name.toLowerCase())
          )
          .map((book) => ({
            id: book.id,
            name: book.name,
            publisher: book.publisher,
          })),
      },
    };
  }

  if (reading) {
    const read = reading === "1";
    return {
      status: "success",
      data: {
        books: books
          .filter((book) => book.reading === read)
          .map((book) => ({
            id: book.id,
            name: book.name,
            publisher: book.publisher,
          })),
      },
    };
  }
  if (finished) {
    const finish = finished === "1";
    return {
      status: "success",
      data: {
        books: books
          .filter((book) => book.finished === finish)
          .map((book) => ({
            id: book.id,
            name: book.name,
            publisher: book.publisher,
          })),
      },
    };
  }
  return {
    status: "success",
    data: {
      books: books.map((book) => ({
        id: book.id,
        name: book.name,
        publisher: book.publisher,
      })),
    },
  };
};
module.exports = getAllBooksHandler;
