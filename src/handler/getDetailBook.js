const books = require("../books");

const getDetailBookHandler = (request, h) => {
  const { bookId } = request.params;

  const book = books.filter((n) => n.id === bookId)[0];

  if (typeof book !== 'undefined') {
    return h.response({
      status: "success",
      data: {
        book,
      },
    });
  }
  const response = h.response({
    status: "fail",
    message: "Buku tidak ditemukan",
  });
  response.code(404);
  return response;
};

module.exports = getDetailBookHandler;
