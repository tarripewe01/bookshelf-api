const addBookHandler = require("./src/handler/addBook");
const getAllBooksHandler = require("./src/handler/getAllBooks");
const getDetailBookHandler = require("./src/handler/getDetailBook");
const editBookByIdHandler = require("./src/handler/editBook");
const deleteBookByIdHandler = require("./src/handler/deleteBook");

const routes = [
  {
    method: "POST",
    path: "/books",
    handler: addBookHandler,
  },
  {
    method: "GET",
    path: "/books",
    handler: getAllBooksHandler,
  },
  {
    method: "GET",
    path: "/books/{bookId}",
    handler: getDetailBookHandler,
  },
  {
    method: "PUT",
    path: "/books/{bookId}",
    handler: editBookByIdHandler,
  },
  {
    method: "DELETE",
    path: "/books/{bookId}",
    handler: deleteBookByIdHandler,
  },
];

module.exports = routes;
